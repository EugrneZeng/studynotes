# 一个HTTP事务的剖析 #

## 原文：《[Anatomy of an HTTP Transaction](https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction/ "Anatomy of an HTTP Transaction")》 ##

这份指引的目的是在与传授一个对Node.js的HTTP处理的全面认识。我们将假设你知道，在一般意义上，无论什么环境或者编程语言，HTTP是如何工作的。我们也假设你有一点点熟悉Node.js的[EventEmitters](https://nodejs.org/api/events.html)和[Streams](https://nodejs.org/api/stream.html)。如果你并不是很熟悉它们，花点时间去浏览下他们的API文档是值得的。

### 创建服务器 ###

任何Node的网页服务器应用都将在某一时刻必须创建一个网页服务器（web server）对象，这是通过[createServer](https://nodejs.org/api/http.html#http_http_createserver_requestlistener "createServer")来完成的。

```js
const http = require('http');

const server = http.createServer((request, response) => {
  // magic happens here!
});
```

传给[createServer](https://nodejs.org/api/http.html#http_http_createserver_requestlistener "createServer")的函数会针对该服务器的每个request都被调用一次。事实上，[createServer](https://nodejs.org/api/http.html#http_http_createserver_requestlistener "createServer")返回的[Server](https://nodejs.org/api/http.html#http_class_http_server "Server")对象是一个[EventEmitter](https://nodejs.org/api/events.html#events_class_eventemitter "EventEmitter")，而我们在这里做的只是，创建一个`server`对象，然后添加监听器的简写。

```js
const server = http.createServer();
server.on('request', (request, response) => {
  // the same kind of magic happens here!
});
```

当一个HTTP请求到达这个服务器，Node就会用几个方便的对象来调用这个请求处理器函数来处理事务，那就是`request`和`response`。我们待会儿将会谈到。

为了真正地服务请求，[listen](https://nodejs.org/api/http.html#http_server_listen_port_hostname_backlog_callback)方法必须在`server`对象上被调用。在大多数情况下，你要做的只是给`listen`方法传入一个你的服务器需要监听的端口号。当然也有其他的选项，参考[API文档](https://nodejs.org/api/http.html)。

### 方法，URL和头 ###

当正在处理一个请求的时候，你可能将要做的第一件事情就是看看请求的方法和URL，这样才能采取合适的行动。Node通过在request对象上放置一些方便的属性，使这相对容易些。

```js
const { method, url } = request;
```

**请注意**：`request`对象是[IncomingMessage](https://nodejs.org/api/http.html#http_class_http_incomingmessage)的一个实例。

这里的`method`将一直都是HTTP的方法/动词，`url`是完整的URL，但不包含服务器，协议和端口号。这对于一个典型的URL，意味着就是之后的所有内容，包括第三个正斜杠。

Headers也在不远处。他们就在他们自己的一个在`request`上面叫做`headers`的对象里。

```js
const { headers } = request;
const userAgent = headers['user-agent'];
```

这里需要重点注意的是，所有的头信息都是用小写来表示的，不管客户端是如何发送的。无论基于何种目的，这使转化头信息的人物更加简单化了。

如果一些头信息是重复的，那么它们的值将会被覆盖，或者用连接在一起成为逗号分割的字符串，视乎头信息。在一些情况下，这可能是有问题的，所以也提供原始的头信息[rawHeaders](https://nodejs.org/api/http.html#http_message_rawheaders)。

### 请求体 ###

当我们接收到一个`POS`或者`PUT`的请求，对于你的应用来说，请求体可能是很重要的。获取请求体的数据比获取头信息要稍微复杂一些。传给请求处理器的`request`对象实现了[ReadableStream](https://nodejs.org/api/stream.html#stream_class_stream_readable)接口。这个stream可以监听或者pipe到任何地方，就像其他任何stream一样。我们可以通过监听stream的`data`和`end`事件，来从stream里攫取出数据。

在每个`data`事件里面触发出来的chunk是一个[Buffer](https://nodejs.org/api/buffer.html)，如果你知道它将会是一个字符串数据，最好的办法就是把这些数据收集到一个数组里，然后在`end`事件里，连接起来然后stringify一下。

```js
let body = [];
request.on('data', (chunk) => {
  body.push(chunk);
}).on('end', () => {
  body = Buffer.concat(body).toString();
  // at this point, `body` has the entire request body stored in it as a string
});
```

**请注意**：这看上去可能有点乏味，在很多情况下，幸运的是，有一些模块像[concat-stream](https://www.npmjs.com/package/concat-stream)和[body](https://www.npmjs.com/package/body)在[npm](https://www.npmjs.com/)里，可以帮助我们隐藏掉一些逻辑。在那之前，对这些有个好一点的认识是很重要的，这也是为什么你会在这里。

### 关于错误的小提示 ###

因为`request`是一个[ReadableStream](https://nodejs.org/api/stream.html#stream_class_stream_readable)，它也就是一个[EventEmitter](https://nodejs.org/api/events.html#events_class_eventemitter "EventEmitter")，当错误发生的时候，它表现得跟其他一样。

在`request`流里面的一个错误会通过在流上emmit一个`error`事件来表现它自己。**如果你没有为这个事件设置一个监听器，那么这个错误将会被抛出，那将会使你的node应用崩溃。**你应该因此在request流上添加一个`error`的监听器，即使你只是记录以下它然后继续你该做的事情。（其实你可能最好发回去一个比如HTTP的错误返回，稍后会讨论到。）

```js
request.on('error', (err) => {
  // This prints the error message and stack trace to `stderr`.
  console.error(err.stack);
});
```

这些[错误的处理](https://nodejs.org/api/errors.html)，有其他办法，比如其他抽象或者工具。但应该一直注意那些错误会且一定会发生，而你将不得不去处理它们。

### 到目前为止我们有什么了？ ###

在这里，我们已经覆盖了一个服务器的创建，以及从request里攫取方法（动词），URL，请求头和请求体。当我们把它们放在一起，那就可能会像这样：

```js
const http = require('http');

http.createServer((request, response) => {
  const { headers, method, url } = request;
  let body = [];
  request.on('error', (err) => {
    console.error(err);
  }).on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
    body = Buffer.concat(body).toString();
    // At this point, we have the headers, method, url and body, and can now
    // do whatever we need to in order to respond to this request.
  });
}).listen(8080); // Activates this server, listening on port 8080.
```

如果我们运行这个例子中的代码，我们将可以*接收*请求，但不会*响应*它们。事实上，如果你在web浏览器里面访问这个示例，你的请求将会超时，只因什么都没有被发送回去给客户端。

到目前为止我们一点都没有接触到`response`对象，那是[ServerResponse](https://nodejs.org/api/http.html#http_class_http_serverresponse)的一个实例，是一个[WritableStream](https://nodejs.org/api/stream.html#stream_class_stream_writable)。它包含了很多有用的方法，用来发送数据回去给客户端，我们将在稍后谈到它。

### HTTP状态码 ###

如果你不去设置它，响应上的状态码将始终是200。当然不是所有的HTTP响应都刚好是它，有时候你确实想设置不同的状态码。那么你就可以设置`statusCode`属性：

```js
response.statusCode = 404; // Tell the client that the resource wasn't found.
```

这里还有一些捷径，我们很快就会看到。

### 响应头的设置 ###

头部信息可以通过一个叫[setHeader](https://nodejs.org/api/http.html#http_response_setheader_name_value)的很方便的方法来设置。

```js
response.setHeader('Content-Type', 'application/json');
response.setHeader('X-Powered-By', 'bacon');
```

当在一个response上设置头信息的时候，它们的名字大小写是不敏感的。如果你重复设置一个头信息，你设置的最后一个值将是最终被发送的那个。

### 显式地发送数据头 ###

刚刚讨论过的设置头信息和状态码的方法假设你正在使用“隐式头”。这意味着在开始发送主体数据之前，你在指望node在正确的时间为你发送报头。

如果你要，你可以*显式*地把头信息写在响应流上。为此，有一个叫[writeHead](https://nodejs.org/api/http.html#http_response_writehead_statuscode_statusmessage_headers)的方法，用来写状态码和头信息到流上。

```js
response.writeHead(200, {
  'Content-Type': 'application/json',
  'X-Powered-By': 'bacon'
});
```

一旦你设置了头信息（不管是显式的还是隐式的），你就准备好开始发送响应体数据了。

### 响应体的发送 ###

由于`response`对象是一个[WritableStream](https://nodejs.org/api/stream.html#stream_class_stream_writable)，给客户端写一个响应体只不过是一个常用的流方法使用的问题。

```js
response.write('<html>');
response.write('<body>');
response.write('<h1>Hello, World!</h1>');
response.write('</body>');
response.write('</html>');
response.end();
```

流（stream）上的`end`方法也可以传入一些可选的数据，作为流上的最后一片数据。因此我们可以简单地把上面的示例写成这样：

```js
response.end('<html><body><h1>Hello, World!</h1></body></html>');
```

**请注意**：在写数据块到响应体之前设置状态码和头信息是很重要的。这很合理，因为在HTTP响应里头信息在响应体之前。

### 关于错误的另一个小提示 ###

`response`流也可以触发（emit）`error`事件，还有时候你将不得不要去处理它们。关于`request`流的错误的忠告同样适用于此。

### 合起来看 ###

现在我们已经学会了做HTTP响应，让我们合起来看。基于早先的示例，我们要做一个服务器，把来自客户端发送过来的数据发回去给客户端。我们将把那些数据用`JSON.stringify`格式化成JSON字符串。

```js
const http = require('http');

http.createServer((request, response) => {
  const { headers, method, url } = request;
  let body = [];
  request.on('error', (err) => {
    console.error(err);
  }).on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
    body = Buffer.concat(body).toString();
    // BEGINNING OF NEW STUFF

    response.on('error', (err) => {
      console.error(err);
    });

    response.statusCode = 200;
    response.setHeader('Content-Type', 'application/json');
    // Note: the 2 lines above could be replaced with this next one:
    // response.writeHead(200, {'Content-Type': 'application/json'})

    const responseBody = { headers, method, url, body };

    response.write(JSON.stringify(responseBody));
    response.end();
    // Note: the 2 lines above could be replaced with this next one:
    // response.end(JSON.stringify(responseBody))

    // END OF NEW STUFF
  });
}).listen(8080);
```

### 回显服务器的示例 ###

让我们简单化一下刚刚的示例，让它成为一个回显服务器，也就是只在response里发送从request里接收到的任何数据。我们所要做的是从request流里攫取数据，然后把数据写到response流里去，与我们在前个例子里做的类似。

```js
const http = require('http');

http.createServer((request, response) => {
  let body = [];
  request.on('data', (chunk) => {
    body.push(chunk);
  }).on('end', () => {
    body = Buffer.concat(body).toString();
    response.end(body);
  });
}).listen(8080);
```

现在我们来改进下这个。我们要仅当如下条件的时候发送回显信息：

* requst方法是POST；
* URL是`/echo`

其他情况下，我们就简单地响应404回去。

```js
const http = require('http');

http.createServer((request, response) => {
  if (request.method === 'POST' && request.url === '/echo') {
    let body = [];
    request.on('data', (chunk) => {
      body.push(chunk);
    }).on('end', () => {
      body = Buffer.concat(body).toString();
      response.end(body);
    });
  } else {
    response.statusCode = 404;
    response.end();
  }
}).listen(8080);
```

**注意**：通过这样来检查URL，我们其实是实现了一种形式的“路由”。其他形式的路由简单的可以通过`switch`语句，或者复杂的就通过一整个框架例如[express](https://www.npmjs.com/package/express)。如果你想找一些东西只做路由没别的，那么可以试试[router](https://www.npmjs.com/package/router)。

好了，让我们来简化一下。请记住，request对象是一个[ReadableStream](https://nodejs.org/api/stream.html#stream_class_stream_readable)而response对象是一个[WritableStream](https://nodejs.org/api/stream.html#stream_class_stream_writable)。这意味着我们可以用[pipe](https://nodejs.org/api/stream.html#stream_readable_pipe_destination_options)来使数据从一个直通到另一个。这也正是我们需要一个回显服务器做的事情：

```js
const http = require('http');

http.createServer((request, response) => {
  if (request.method === 'POST' && request.url === '/echo') {
    request.pipe(response);
  } else {
    response.statusCode = 404;
    response.end();
  }
}).listen(8080);
```

耶！streams。

然而我们还没有完成哦。就像在这份指南里提到过多次的一样，错误会也确实会发生，我们需要去处理它们。

为了在request流里处理错误，我们会把错误记录到`stderr`，然后发送一个404状态码来表示这是一个`Bad Request`。在一个真实的应用程序里，尽管我们希望检查错误，来指出正确的状态码和消息是什么。和通常的错误一样，你应该参考下[`Error` documentation](https://nodejs.org/api/errors.html)。

在response上，我们将仅仅把错误记录到`stderr`。

```js
const http = require('http');

http.createServer((request, response) => {
  request.on('error', (err) => {
    console.error(err);
    response.statusCode = 400;
    response.end();
  });
  response.on('error', (err) => {
    console.error(err);
  });
  if (request.method === 'POST' && request.url === '/echo') {
    request.pipe(response);
  } else {
    response.statusCode = 404;
    response.end();
  }
}).listen(8080);
```

现在我们已经介绍了处理HTTP请求的大多数基础知识。在这里，你应该可以：

* 用一个request处理函数实例化一个HTTP服务器，让它监听一个端口。
* 从request对象里获取头信息，URL，方法和请求体数据。
* 根据request对象中的URL和/或其他数据做出路由决策。
* 通过reponse对象发送头信息，HTTP状态码和响应体数据。
* 从request对象到response对象输送数据（pipe data）。
* 在request流和reponse流上处理流错误。

从这些基础知识里，许多常规用途的Node.js HTTP服务器就可以构建起来了。还有很多这些API提供的其他事情，所以请务必浏览下关于[EventEmitters](https://nodejs.org/api/events.html)，[Streams](https://nodejs.org/api/stream.html)，和[HTTP](https://nodejs.org/api/http.html)的API文档。
